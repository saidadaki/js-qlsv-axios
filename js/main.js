const BASE_URL = "https://63f442dcfe3b595e2ef03b89.mockapi.io";

// axios({
//     url: `${BASE_URL}/qlsv`,
//     method: "GET",
// }).then(function(res) {
//     console.log(res);
//     var dssv = res.data;
//     renderDSSV(dssv);
// }).catch(function(err){
//     console.log("err", err);
// })

// fetch axios 
function fetchDSSV() {
    batLoading()
    axios({
        url: `${BASE_URL}/qlsv`,
        method: "GET",
    }).then(function(res) {
        var dssv = res.data;
        tatLoading()
        renderDSSV(dssv);
    }).catch(function(err){
        tatLoading()
        console.log("err",err);
    })
}

fetchDSSV();

// render dssv 
function renderDSSV(dssv) {
    var contentHTML = '';
    for (var i = dssv.length - 1; i >= 0; i--) {
      var item = dssv[i];
      var contentTr = `
      <tr>
          <td> ${item.ma} </td>
          <td> ${item.ten} </td>
          <td> ${item.email} </td>
          <td> 0 </td>
          <td>
               <button 
               onclick="xoaSV('${item.ma}')"
               class="btn btn-danger">
               Xoá
               </button>
                 <button 
               onclick="suaSV('${item.ma}')"
               class="btn btn-warning">
               Sửa
               </button>
          </td>
      </tr>
      `;
      contentHTML += contentTr;
    }
    document.getElementById('tbodySinhVien').innerHTML = contentHTML;
}

// xoa SV 
function xoaSV(id) {
    axios({
        url: `${BASE_URL}/qlsv/${id}`,
        method: "DELETE",
    }).then(function(res){
        fetchDSSV();
    }).catch(function err() {
        console.log("err", err);
    })
}

// sua SV 
function suaSV(id) {
    // get detail from server 
    axios({
        url: `${BASE_URL}/qlsv/${id}`,
        method: "GET",
    }).then(function (res) {
        console.log("detail by id",id, res.data);
        showThongTinLenForm(res.data);
    }).catch(function (err) {
        console.log("err", err);
    })
}

function capNhatSV() {
    var sv = layThongTinTuForm();
    console.log("sv", sv);
    axios({
        url: `${BASE_URL}/qlsv/${sv.ma}`,
        method: "PUT",
        data: sv,
    }).then(function (res) {
        console.log(res);
        fetchDSSV();
    }).catch(function (err) {
        console.log(err);
    })
}

function themSV() {
    var sv = layThongTinTuForm();
    axios({
        url: `${BASE_URL}/qlsv`,
        method: "POST",
        data: sv,
    }).then(function(res) {
        fetchDSSV()
    }).catch(function (err) {
        console.log(err);
    })
}

function batLoading() {
    document.getElementById("loading").style.display = "flex";
}
function tatLoading() {
    document.getElementById("loading").style.display = "none";
}

// reset form 
function resetForm() {
    document.getElementById('formQLSV').reset();
}